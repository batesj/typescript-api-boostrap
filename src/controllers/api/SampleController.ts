import { Request, Response } from "express";

export let getSample = (req: Request, res: Response) => {
  res.json("Success! You have reached the Sample api endpoint.");
};
