import { Router } from "express";
import * as sampleController from "../../controllers/api/SampleController";

const router: Router = Router();
const sampleRoutes: Router = router;

router.get("/", sampleController.getSample);

export default sampleRoutes;
