import { Router } from "express";
import sampleRoutes from "./api/sample";

const router: Router = Router();
const apiRoutes: Router = router;

router.use("/sample", sampleRoutes);

export default apiRoutes;
